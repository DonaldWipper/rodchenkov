import csv
import requests
import xml.etree.ElementTree as ET

def run():
    csvfile =open('nodes.csv')
    readCSV = csv.reader(csvfile, delimiter=',')
    nodes = []
    i = 0

    root = ET.fromstring(requests.get("http://graphonline.ru/cgi-bin/loadGraph.php?name=ZHqOhbbtnoXXyNmE").text)
    list_attr = []
    nodes = []
    minX = 1000.0
    minY = 1000.0
    maxX = -1.0
    maxY = -1.0
    X = 1000.0
    Y = 600.0
    myFile = open('output.txt', 'w')
    for child in root:
        print(child.attrib)
        for a in child:
            print(a.attrib)
            for c in a: 
                if(c.tag == "node"):
                   print(c.attrib)
                   node = {}
                   node["id"] = c.attrib["id"]
                   node["dX"] = float(c.attrib["positionX"])
                   node["dY"] = float(c.attrib["positionY"])
                   if  float(node["dX"]) < float(minX):
                       minX = node["dX"] 
                   if  float(node["dY"]) < float(minY):
                       minY = node["dY"] 
                   if  float(node["dX"]) > float(maxX):
                       maxX = node["dX"] 
                   if  float(node["dY"]) > float(maxY):
                       maxY = node["dY"]  
                   nodes.append(node)
    print(minX)
    for node in nodes:
        node["dX"] = int((-minX + node["dX"]) * X /(maxX - minX))
        node["dY"] = int((-minY + node["dY"]) * Y / (maxY - minY)) 
        myFile.write(str(node["dX"]) + " " +   str(node["dY"])+ "\n") 
    
    for row in readCSV:
        if  i == 0:
            i = 1
            continue;  
        if not row:
            break;
        a = {}
        new_str = '{"label":' + '"' + row[0] + '"'
        new_str = new_str + ', "name":' + '"' + row[1] + '"'
        new_str = new_str + ', "type":' + '"' + row[2].strip() + '"'
        new_str = new_str + ', "id":' +  row[3] 
        new_str = new_str + ', "image":"' +  row[0] + '.png"'
        if i < 24:
            new_str = new_str + ',"dx":' + str(nodes[i-1]["dX"])
            new_str = new_str + ',"dy":' + str(nodes[i-1]["dY"]) + '},'
        else:
            new_str = new_str + ',"dx":' + str(0)
            new_str = new_str + ',"dy":' + str(0) + '},'
        myFile.write(new_str + "\n") 
        i = i + 1
    myFile.write("\n") 
    myFile.write("\n") 
    matrix = {}
    max = 1
    csvfile = open('edges.csv') 
    readCSV = csv.reader(csvfile, delimiter=',')
    nodes = []
    i = 0
    for row in readCSV:
        if  i == 0:
            i = 1
            continue;  
        if not row:
            break;
        a = {}
        
        if int(row[0]) > max:
            max = int(row[0])
        if int(row[1]) > max:
            max = int(row[1])
        el = str(int(row[0])) + "," + str(int(row[1]))
        matrix[el] = 1
        new_str = '{"source":' + row[0] + ','
        new_str = new_str + '"target":' + row[1] + ','
        new_str = new_str + '"type":' + '"' + row[2] + '",'
        new_str = new_str + '"type_edge":' + '"' + row[3].strip() + '"},' 
        myFile.write(new_str+ "\n")     
    myFile.write("\n") 
    myFile.write("\n")
    for i in range(1, max+1):
       for j in range(1, max+1):
           el =  str(i) 
           el = el +   "," 
           el = el + str(j)
           if el in matrix:
               if j != max: 
                   myFile.write("1,")
               else:
                   myFile.write("1")
           else:
               if j != max: 
                   myFile.write("0,")
               else:
                   myFile.write("0")
       myFile.write("\n")
    myFile.close()
run()      










